#include <SpeedyStepper.h>


#define MOTOR_STEP  10
#define MOTOR_DIRECTION  9
#define MOTOR_ENABLE  8
#define LIMIT_SWITCH  4
#define WAITING_POSITION -10 // mm
#define WAITING_POSITION_CORRECTION 3 // mm

#define MOVE_UP 7
#define MOVE_DOWN 6

#define MOTOR_SPEED 3  //mm/c
#define MOTOR_ACCELERATION 3  //mm/c2
#define STEP_DIV 8
//M5 : 0.8mm
#define homingSpeedInMMPerSec  5.0
#define maxHomingDistanceInMM  30.0   // since my lead-screw is 38cm long, should never move more than that
#define directionTowardHome  -1        // direction to move toward limit switch: 1 goes positive direction, -1 backward

#define PELTIER_MODULE 3
#define PELTIER_ALWAYS_ON 0
#define PELTIER_MOTOR_UP 1

uint8_t peltierMode = PELTIER_ALWAYS_ON;

SpeedyStepper stepper;


#define MASTERBUTTON 2
#define CALIBR_WAIT 5000


//#############   OLED   #################################
// Edit AVRI2C_FASTMODE in SSD1306Ascii.h to change the default I2C frequency.
#include "SSD1306Ascii.h"
#include "SSD1306AsciiAvrI2c.h"
// 0X3C+SA0 - 0x3C or 0x3D
#define I2C_ADDRESS 0x3C
SSD1306AsciiAvrI2c oled;
//------------------------------------------------------------------------------
#define DEBUG 0


uint8_t masterButtonState = 0;
uint8_t masterButtonState_prev = 0;
uint8_t masterButtonState_prev_prev = 0;
uint8_t moveUpButtonState = 0;
uint8_t moveDownButtonState = 0;

unsigned long masterButtonLastChage = 0;

void setup() {
#if DEBUG
  Serial.begin(9600);
#endif
  pinMode(PELTIER_MODULE, OUTPUT);
  pinMode(MASTERBUTTON, INPUT_PULLUP);
  pinMode(MOVE_UP, INPUT_PULLUP);
  pinMode(MOVE_DOWN, INPUT_PULLUP);
  

  oled.begin(&Adafruit128x64, I2C_ADDRESS);
  // Call oled.setI2cClock(frequency) to change from the default frequency.

  oled.setFont(System5x7);
  oled.clear();
  oled.print("Hello, MrFrost");
  masterButtonState_prev_prev = masterButtonState_prev;
  masterButtonState_prev = masterButtonState;
  masterButtonState = digitalRead(MASTERBUTTON);

  pinMode(LIMIT_SWITCH, INPUT_PULLUP);
  stepper.connectToPins(MOTOR_STEP, MOTOR_DIRECTION);
  //M5 : 0.8mm - 360

  stepper.setStepsPerMillimeter(250 * STEP_DIV);    // 1x microstepping
  stepper.setSpeedInMillimetersPerSecond(MOTOR_SPEED);
  stepper.setAccelerationInMillimetersPerSecondPerSecond(MOTOR_ACCELERATION);

if (peltierMode == PELTIER_ALWAYS_ON) digitalWrite(PELTIER_MODULE, HIGH);
}


#define MODE_WAIT 0
#define MODE_CALIBR 1
#define MODE_START 2
#define MODE_STOP 3


uint8_t activeMode = MODE_WAIT;

unsigned long MrFrostClock = 0;
unsigned long displayUpdateTimer = 0;

float resultTime = 0;

void readButtons() {
  masterButtonState_prev_prev = masterButtonState_prev;
  masterButtonState_prev = masterButtonState;
  masterButtonState = digitalRead(MASTERBUTTON);
  moveUpButtonState = digitalRead(MOVE_UP);
  moveDownButtonState = digitalRead(MOVE_DOWN);
  
#if DEBUG
  Serial.print(masterButtonState_prev_prev);
  Serial.print(masterButtonState_prev);
  Serial.print(masterButtonState);
  Serial.print(moveUpButtonState);
  Serial.println(moveDownButtonState);
  
#endif

  
}

void loop() {
  readButtons();
  if(moveUpButtonState == 1){
    //stepper.moveRelativeInMillimeters(1);
    stepper.moveToHomeInMillimeters(directionTowardHome, MOTOR_SPEED, 1, LIMIT_SWITCH);

    }
  if(moveDownButtonState == 1){
    stepper.moveRelativeInMillimeters(-1);
    }
  

  if (masterButtonState == 1 && masterButtonState_prev == 0) {
    masterButtonLastChage = millis();
  }
  if (masterButtonState == 1 && masterButtonState_prev == 1 && masterButtonState_prev == 1) {
    activeMode = MODE_CALIBR;
  }
  if (masterButtonState == 0 && activeMode == MODE_CALIBR) {
    activeMode = MODE_WAIT;
  }
  if (masterButtonState == 1 && activeMode == MODE_CALIBR && millis() - masterButtonLastChage > CALIBR_WAIT) {
    oled.clear();
    oled.print("Calibr");
#if DEBUG
    Serial.println("Calibr");
#endif
    activeMode = MODE_CALIBR;
    startCalibr();
    //oled.clear();
    oled.print(" - ready");
#if DEBUG
    Serial.println("ready");
#endif
    activeMode = MODE_WAIT;
    readButtons();
  }

  else if (masterButtonState == 0 && masterButtonState_prev == 1 && activeMode != MODE_CALIBR) {
    if (activeMode == MODE_WAIT) {
      activeMode = MODE_START;
      oled.clear();
      oled.print("up");
#if DEBUG
      Serial.println("up");
#endif
      welcomeMrFrost();
      MrFrostClock = millis();
      //oled.clear();
      oled.print(" - start");
#if DEBUG
      Serial.println("start");
#endif

    }
    else if (activeMode == MODE_START) {

      resultTime = (millis() - MrFrostClock) / 1000.;
      activeMode = MODE_WAIT;
      bybyMrFrost();
      oled.clear();
      oled.print("DURATION:\n-----------\n" + String(resultTime) + " s");
#if DEBUG
      Serial.println(String(resultTime) + " s");
#endif
    }
  }
  if (activeMode == MODE_START && millis() - displayUpdateTimer > 1000) {
    oled.clear();
    oled.print('\n' + String((millis() - MrFrostClock) / 1000.) + " s");
    displayUpdateTimer = millis();
  }
  
  delay(200);        // delay in between reads for stability

}



void startCalibr() {

  if (stepper.moveToHomeInMillimeters(directionTowardHome, homingSpeedInMMPerSec, maxHomingDistanceInMM, LIMIT_SWITCH) != true)
  {
    //
    // this code is executed only if homing fails because it has moved farther
    // than maxHomingDistanceInMM and never finds the limit switch, blink the
    //
    oled.print(" - homing ERR");
  }
  else {
    stepper.moveToPositionInMillimeters(WAITING_POSITION);
  }
}
void welcomeMrFrost() {
  if (peltierMode == PELTIER_MOTOR_UP) digitalWrite(PELTIER_MODULE, HIGH);
  //stepper.moveToPositionInMillimeters(0);
  if (stepper.moveToHomeInMillimeters(directionTowardHome, MOTOR_SPEED, -WAITING_POSITION+WAITING_POSITION_CORRECTION, LIMIT_SWITCH) != true)
  {
    //
    // this code is executed only if homing fails because it has moved farther
    // than maxHomingDistanceInMM and never finds the limit switch, blink the
    //
    oled.print("Lock");
  }
}


void bybyMrFrost() {
  if (peltierMode == PELTIER_MOTOR_UP) digitalWrite(PELTIER_MODULE, LOW);
  stepper.moveToPositionInMillimeters(WAITING_POSITION);
}
